﻿using System;

namespace week_3_exercise_16
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("*******************************");
            Console.WriteLine("**** Welcome to my app ****");
            Console.WriteLine("*******************************");
            Console.WriteLine("");

            Console.WriteLine("*******************************");
            Console.WriteLine("What is your name?");
            var name = Console.ReadLine();
            Console.WriteLine("");
            Console.WriteLine($"Your name is: {name}");

            Console.ResetColor();
            Console.WriteLine();
            Console.WriteLine("Press <Enter> to exit this program");
            Console.ReadKey();
        }
    }
}
